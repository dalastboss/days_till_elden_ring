Clone this repo onto your machine using the command:

    $ git clone https://gitlab.com/dalastboss/days_till_elden_ring

Then, run the commands

    $ cd days_till_elden_ring
    $ ./install.sh

Finally, check how many days you need to wait until Elden Ring by running

    $ days_till_elden_ring
