open Unix

type month = Jan | Feb | Other

let year time = time.tm_year + 1900

let month time =
  match time.tm_mon with
  | 0 -> Jan
  | 1 -> Feb
  | _ -> Other

let main () =
  let time = localtime (time ()) in
  let year = year time in
  let days = match month time with
    | Jan -> 25 + (31 - time.tm_mday)
    | Feb -> 25 - time.tm_mday
    | Other -> -1
  in
  if year > 2022 || days <= 0 then
    Printf.printf "It's out bitch.\n"
  else
    Printf.printf "%d days till elden ring.\n" days

let _ = main ()
