#!/bin/bash

echo "Checking if ocaml package manager is installed..."

if ! [ -x "$(command -v opam)" ] ; then
  echo "Error! Please install opam..."
  echo "Visit https://opam.ocaml.org/doc/Install.html and try again."
  exit 1
fi

echo "Checking if ocaml tools are installed..."

if ! [ -x "$(command -v ocamlbuild)" ]; then
  echo "Error! Couldn't find ocamlbuild..."
  echo "Run \"opam install utop\" and try again."
  exit 1
fi

echo "Building executable..."

if ! ocamlbuild -pkg unix days_till_elden_ring.native ; then
  echo "Error while building executable."
  exit 1
fi

echo "Attempting to install executable..."

if ! [ -d "/usr/local/bin" ] ; then
  echo "/usr/local/bin does not exist..."
  echo "Run \"sudo mkdir /usr/local/bin\" and try again"
  exit 1
fi

if ! [[ ":$PATH" == *":/usr/local/bin:"* ]]; then
  echo "Your PATH is missing /usr/local/bin, you should add it."
  exit 1
fi

if ! sudo cp days_till_elden_ring.native /usr/local/bin/days_till_elden_ring ; then
  echo "Couldn't install executable."
  exit 1
fi

echo "Installation complete."
